<?php

/**
 * Implements hook_views_data().
 */
function media_filter_views_data() {
  $data = [];
  $data['media_field_data']['media_filter_entity'] = [
    'title' => 'Media filter entity',
    'filter' => [
      'title' => 'Media filter entity',
      'id'    => 'media_filter_entity',
    ],
  ];
  return $data;
}
