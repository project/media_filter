<?php

namespace Drupal\media_filter\Plugin\views\filter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Markup;
use Drupal\node\Entity\Node;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\Core\Url;
use Drupal\views\Views;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Database\Query\Condition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * @ViewsFilter("media_filter_entity")
 */
class MediaFilterEntity extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state) {
    $entity_types = \Drupal::entityTypeManager()->getDefinitions();
    $allowedEntities = [];
    foreach ($entity_types as $key => $type) {
      switch ($key) {
        case 'node':
        case 'taxonomy_term':
          $bundle = $type->getBundleEntityType();
          $allowedEntities[$key] = [];
          $types = \Drupal::entityTypeManager()->getStorage($bundle)->loadMultiple();
          foreach ($types as $bundle) {
            $allowedEntities[$key][$key . '.' . $bundle->id()] = $bundle->label();
          }
          break;
      }
    }
    $options = [
      '' => $this->t('All'),
    ];
    $options = array_merge($options, $allowedEntities);
    $form['media_filter_entity'] = [
      '#type' => 'select',
      '#title' => $this->t('Parent entity type'),
      '#options' => $options,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function acceptExposedInput($input) {
    $accept = FALSE;
    if (!empty($input['media_filter_entity'])) {
      $accept = TRUE;
    }
    return $accept;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $request = \Drupal::request()->query->all();
    if (!empty($request['media_filter_entity'])) {
      [$type, $bundle] = explode('.', $request['media_filter_entity']);
      $fields = \Drupal::getContainer()->get('entity_field.manager')->getFieldDefinitions($type, $bundle);
      $data = [];
      foreach ($fields as $key => $field) {
        $settings = $field->getSettings();
        if (isset($settings['target_type']) && $settings['target_type'] === 'media') {
          $tables = \Drupal::entityTypeManager()->getStorage('node')->getTableMapping()
            ->getAllFieldTableNames($field->getName());
          $data[$key] = $tables;
        }
      }
      $definition = \Drupal::entityTypeManager()->getDefinition($type);
      $id = $definition->getKey('id');

      $queries = [];
      foreach ($data as $table) {
        $table = $table[0];
        $query = \Drupal::database()->select($definition->getBaseTable(), 'entity');
        $query->condition('entity.' . $definition->getKey('bundle'), $bundle);
        $field_name = explode('__', $table)[1];
        $query->innerJoin($table, $field_name, $field_name . ".entity_id = entity.$id");
        $query->addField($field_name, $field_name . '_target_id', 'media_id');
        $queries[] = $query;
      }
      if (!empty($queries)) {
        $getQuery = NULL;
        foreach ($queries as $subQuery) {
          if (is_null($getQuery)) {
            $getQuery = $subQuery;
          }
          else {
            $getQuery->union($subQuery, 'ALL');
          }
        }

        $join = Views::pluginManager('join')->createInstance('standard', [
          'type'          => 'INNER',
          'table'         => $getQuery,
          'field'         => 'media_id',
          'left_table'    => 'media_field_data',
          'left_field'    => 'mid',
          'operator'      => '=',
        ]);
        $this->query->addRelationship('filter_entity', $join, 'media_field_data');

        $this->query->addField('media_field_data', 'mid', 'm_mid', ['function' => 'groupby']);
        $this->query->addGroupBy("media_field_data.mid");

      }
      else {
        $this->view->executed = TRUE;
      }

    }
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    return 'Media filter entity';
  }
}
